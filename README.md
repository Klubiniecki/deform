## About

Sandbox form project. Check it out at https://deform.herokuapp.com/.

#### What to expect?

- Simple, responsive form
- Redux with Redux-thunk for data flow
- SASS to add some style!

#### What's next?

- set the project up with `yarn` / `npm install`
- spin it up with `yarn start` / `npm start` and go to http://localhost:3000/
- test with `yarn test` / `npm test`

-x add a button
-x onClick button opens an upload window
-x when file is selected, we want to check for file type (jpg, png)
-x when file is selected, user can see a preview

- preview - rectangle next to the upload button
- user has 2 options: select a different file or submit this one
- add image data to state
- on form submit it should submit the new data (image)
- test for the new functionality
