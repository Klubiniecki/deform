import { SUBMIT_FORM } from "../actions/submit";

const initialState = {
  formData: {
    user: "",
    email: "",
    message: ""
  }
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUBMIT_FORM:
      return {
        ...state,
        formData: action.data
      };

    default:
      return state;
  }
};

export default reducer;
