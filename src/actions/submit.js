export const SUBMIT_FORM = "SUBMIT_FORM";

export const submit = data => ({
  type: "SUBMIT_FORM",
  data
});

export function submitForm(data) {
  return dispatch => {
    setTimeout(() => {
      dispatch(submit(data));
    }, 3000);
  };
}
