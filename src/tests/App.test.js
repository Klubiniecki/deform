import * as React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import thunk from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import App from "../components/App/App";
import Loader from "../components/Loader/Loader";
import reducer from "../reducers/reducer";
import { submit, SUBMIT_FORM } from "../actions/submit";

Enzyme.configure({ adapter: new Adapter() });

const store = createStore(reducer, applyMiddleware(thunk));
const app = shallow(<App store={store} />).dive();

const testData = {
  user: "John Snow",
  email: "john@winterfell.com",
  message: "I know nothing!"
};

describe("Testing form submission.", () => {
  it("Store updates after submit action is dispatched.", () => {
    store.dispatch(submit(testData));
    expect(store.getState().formData.user).toBe("John Snow");
  });

  it("Form should render loader when submitted.", () => {
    app.setState({ loading: true });
    expect(app.find(Loader).length).toBe(1);
  });

  it("Reducer should handle SUBMIT_FORM action.", () => {
    expect(
      reducer(store.getState(), {
        formData: testData,
        type: SUBMIT_FORM
      })
    ).toEqual({
      formData: testData
    });
  });

  it("Renders snapshot.", () => {
    expect(app).toMatchSnapshot();
  });
});
