import React from "react";

const Loader = () => (
  <div className="loader">
    <svg className="circular" viewBox="25 25 50 50">
      <circle
        className="path1"
        cx="50"
        cy="50"
        r="20"
        fill="none"
        stroke="#fafafa"
        strokeWidth="2"
        strokeMiterlimit="10"
      />
      <circle
        className="path2"
        cx="50"
        cy="50"
        r="15"
        fill="none"
        stroke="#7746e8"
        strokeWidth="3"
        strokeMiterlimit="10"
      />
      <circle
        className="path3"
        cx="50"
        cy="50"
        r="10"
        fill="none"
        stroke="#fafafa"
        strokeMiterlimit="10"
      />
    </svg>
  </div>
);

export default Loader;
