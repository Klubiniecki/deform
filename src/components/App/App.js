import * as React from "react";
import { connect } from "react-redux";
import { submitForm } from "../../actions/submit";
import Card from "../Card/Card";
import Loader from "../Loader/Loader";
import logo from "../../images/logo.png";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        user: "",
        email: "",
        message: ""
      },
      loading: false,
      submitted: false,
      image: ""
    };
  }

  componentWillReceiveProps = nextProps => {
    if (this.props.formData !== nextProps.formData) {
      this.setState({ loading: false, submitted: true });
    }
  };

  onInputChange = event => {
    this.setState({
      formData: {
        ...this.state.formData,
        [event.target.name]: event.target.value
      }
    });
  };

  onImageUpload = event => {
    const image = event.target.files[0];
    const fr = new FileReader();
    fr.addEventListener(
      "load",
      () => {
        debugger;
        fr.result;
        this.setState({ image: fr.result }, () => {
          console.log(this.state);
        });
      },
      false
    );
    fr.readAsDataURL(image);
  };

  onSubmit = event => {
    event.preventDefault();
    this.setState({ loading: true });
    this.props.dispatch(submitForm(this.state.formData));
  };

  render() {
    const { user, email, message } = this.state.formData;

    return (
      <div className="container">
        <Card>
          <form className="form" onSubmit={this.onSubmit}>
            <img className="form__logo" src={logo} alt="logo" />
            <label>
              <p className="form__label">NAME:</p>
              <input
                className="form__input"
                type="text"
                name="user"
                value={user}
                placeholder="It's not how big your pencil is. It's how you write your name."
                onChange={this.onInputChange}
                readOnly={this.state.loading}
                required
              />
            </label>
            <label>
              <p className="form__label">Email:</p>
              <input
                className="form__input"
                type="email"
                name="email"
                value={email}
                placeholder="I don't believe in email. I'm old-fashioned. I prefer calling and hanging up."
                onChange={this.onInputChange}
                readOnly={this.state.loading}
                required
              />
            </label>
            <label>
              <p className="form__label">Message:</p>
              <input
                className="form__input"
                type="text"
                name="message"
                value={message}
                placeholder="I see trees of green, red roses too. I see them bloom for me and you. And I think to myself..."
                onChange={this.onInputChange}
                readOnly={this.state.loading}
              />
            </label>
            <label>
              <p className="form_label">Upload image:</p>
              <input
                type="file"
                name="image"
                accept="image/png, image/jpeg"
                onChange={this.onImageUpload}
              />
            </label>
            {this.state.image !== "" && (
              <img src={this.state.image} alt="Preview" height="100" />
            )}
            <div className="form__submit">
              {this.state.loading && <Loader />}
              {!this.state.loading && (
                <button className="form__button" type="submit">
                  {this.state.submitted ? "Form submitted!" : "Let it go!"}
                </button>
              )}
            </div>
          </form>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  formData: state.formData
});

export default connect(mapStateToProps)(App);
